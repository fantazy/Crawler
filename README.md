![](http://i.imgur.com/KnwDsjO.png)

----------

# **Crawler是一款页面图片抓取工具** #

----------
## 适合人群 ##

- **java swing爱好者,进阶**


--
<kbd>*java -jar Crawler.jar*</kbd>
> window7界面
![](http://i.imgur.com/FUaa6dI.jpg)

> **以 [http://hs.17173.com/news/01162017/103158044.shtml](http://hs.17173.com/news/01162017/103158044.shtml "例子链接") 为例**


> **基本将页面上的图像保存到了本地**

![](http://i.imgur.com/gjqUGvy.jpg)

## 程序参见源代码 ##
## 环境 JDK1.8+##