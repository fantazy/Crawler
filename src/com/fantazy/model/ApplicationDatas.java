package com.fantazy.model;

import com.fantazy.util.SystemTools;

import java.util.HashSet;
import java.util.Set;

public class ApplicationDatas {

	private static String savePath = SystemTools.getUserHome();

	private static HashSet<String> pics = new HashSet<>();

	private static HashSet<String> selectedPics = new HashSet<>();

	public static String getSavePath() {
		synchronized (savePath) {
			return savePath;
		}
	}

	public static void setSavePath(String sp) {
		synchronized (savePath) {
			savePath = (sp == null ? SystemTools.getUserHome() : sp);
		}
	}

	public static HashSet<String> getPics() {
		synchronized (pics) {
			return pics;
		}
	}

	public static void setPics(HashSet<String> urls) {
		synchronized (pics) {
			pics = urls;
		}
	}

	public void setSelectedPics(HashSet<String> sp){
		synchronized (selectedPics){
			selectedPics = sp;
		}
	}
	public static Set<String> getSelectedPics(){
		synchronized (selectedPics){
			return selectedPics;
		}
	}
}
