package com.fantazy.util;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class SwingTools {
	
	public static void LocatedInCenter(Window window , int width ,int height){
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		
		int screenWidth = (int) d.getWidth();
		
		int screenHeight = (int) d.getHeight();
		
		window.setBounds((screenWidth - width)/2, (screenHeight - height)/2, width, height);
	}
	
	
	public static void setSystemLookAndFeel() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException
	{
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	}
}
