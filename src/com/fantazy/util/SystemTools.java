package com.fantazy.util;

import java.io.File;
/**
 * @Author Zhoufan [https://github.com/fantaga].
 * @Date 2016/12/31
 * @Description:
 */
public class SystemTools {

	public static final String getOSName(){
		return "OS : " + System.getProperty("os.name");
	}

	public static final String getOSVersion(){
		return "Version : " + System.getProperty("os.version");
	}


	public static final String getLineSeparator(){
		return System.getProperty("line.separator");
	}

	public static final String getFileSeparator(){
		return File.separator;
	}

	public static final String getUserHome(){
		return System.getProperty("user.home");
	}

	public static final String openDirectoryCommend(String path){
		String commend = "";
		if(getOSName().contains("Windows"))
			commend = "cmd /c start explorer";
		if (getOSName().contains("Linux"))
			commend = "nautilux ";
		if(getOSName().contains("Mac"))
			commend = "";
		else
			throw new RuntimeException("unknown os!");
		return commend + path;

	}
}
