package com.fantazy.util;

//各种控件上的文本
public class TextConstants {

	public static final String MAIN_FRAME_TITLE = "Crawler-GUI";
	public static final String MAIN_FRAME_INPUT_LABLE_TEXT = "请输入页面地址";
	public static final String MAIN_FRAME_BUTTON_GO_LABLE_TEXT = "开始";
	public static final String MAIN_FRAME_BUTTON_LIST_LABLE_TEXT = "下载列表";
	public static final String MESSAGE_EMPTY = "";
	public static final String MAIN_FRAME_SHUTDOWN_DIALOG_MESSAGE = "要退出程序么";
	public static final String MAIN_FRAME_SHUTDOWN_DIALOG_TITLE = "quit?";


	public static final String LISTDIALOG_FRAME_BUTTON_DOWNLOAD_TEXT = "下载选中";
	public static final String LISTDIALOG_FRAME_LABEL_COUNT_1 = "个图片地址";
	public static final String LISTDIALOG_FRAME_CHECKBOX_SELECTALL = "全选";
	public static final String LISTDIALOG_FRAME_CHECKBOX_DISSELECTALL = "反选";
	public static final String LISTDIALOG_FRAME_BUTTON_CHANGEDIR = "更改路径";
	public static final String LISTDIALOG_FRAME_LABEL_GET = "本次收获了";
	public static final String LISTDIALOG_FRAME_LABEL_CHOOSE= "您选择了";
	public static final String LISTDIALOG_FRAME_BUTTON_OPEN= "打开文件夹";

	public static final String LISTDIALOG_FRAME_TITLE = "LIST";

	public static final String DOWNLOAD_PROGRESSDIALOG_TITLE = "downloading...";
}
