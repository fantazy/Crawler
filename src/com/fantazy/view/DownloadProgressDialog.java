package com.fantazy.view;

import com.fantazy.task.DownLoadWorker;
import com.fantazy.util.SwingTools;
import com.fantazy.util.TextConstants;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class DownloadProgressDialog extends JDialog implements WindowListener{

	public static JProgressBar progressBar;
	private static final long serialVersionUID = 1L;
	public DownloadProgressDialog(ListDialog parent)
	{	
		super(parent, TextConstants.DOWNLOAD_PROGRESSDIALOG_TITLE, true);
		progressBar = new JProgressBar(0,100);
		progressBar.setStringPainted(true);
		getContentPane().add(progressBar);
		requestFocus();
		addWindowListener(this);
		SwingTools.LocatedInCenter(this, 300, 50);
		setVisible(true);
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		DownLoadWorker dw = new DownLoadWorker(this);
		dw.execute();
	}
	@Override
	public void windowClosing(WindowEvent e) {
	}
	@Override
	public void windowClosed(WindowEvent e) {
	}
	@Override
	public void windowIconified(WindowEvent e) {
	}
	@Override
	public void windowDeiconified(WindowEvent e) {
	}
	@Override
	public void windowActivated(WindowEvent e) {
	}
	@Override
	public void windowDeactivated(WindowEvent e) {
	}
}
