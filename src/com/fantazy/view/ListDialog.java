package com.fantazy.view;

import com.fantazy.model.ApplicationDatas;
import com.fantazy.util.SwingTools;
import com.fantazy.util.SystemTools;
import com.fantazy.util.TextConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.Iterator;

public class ListDialog extends JDialog implements ItemListener {

	private JPanel contentPane;
	private JLabel titleLabel;
	private JCheckBox checkAllBox;
	private int selectedCount = 0;
	private JCheckBox items[];
	private JPanel listPane;
	private JLabel selectedCountLabel;


	private JButton btnDownload;
	private JPanel bottomPanel;
	private JLabel filePathLabel;
	private DownloadProgressDialog downloadProgressDialog;
	private JButton btnChangeSavePath;
	private JButton btnOpenLocalDir;

	public ListDialog(JFrame parent, boolean model, String title) {
		super(parent, title, model);
		init();
		addActionListener();
		SwingTools.LocatedInCenter(this, 600, 400);
		setVisible(true);
	}

	private void addActionListener() {

		checkboxAllAction();

		btnListAction();

		checkboxItemActin();
		//btnChangeSavepathAction
		btnChangeSavepathAction();
		//btnOpenLocalDir
		btnOpenLocalDirAction();
	}

	private void btnOpenLocalDirAction() {
		btnOpenLocalDir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Runtime.getRuntime().exec(SystemTools.openDirectoryCommend(ApplicationDatas.getSavePath()));
				} catch (IOException e1) {
				}
			}
		});
	}

	private void btnChangeSavepathAction() {
		btnChangeSavePath.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//文件选择器
				JFileChooser fileChooser = new JFileChooser();
				//文件夹可见，文件不能见
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				//展示文件选择器
				int result = fileChooser.showSaveDialog(ListDialog.this);
				if (result == JFileChooser.APPROVE_OPTION) {
					System.out.println(fileChooser.getSelectedFile().getAbsolutePath());
					String savePath = fileChooser.getSelectedFile().getAbsolutePath();
					ApplicationDatas.setSavePath(savePath);
					filePathLabel.setText(savePath);
				}
			}
		});
	}

	/**
	 * 普通checkbox添加事件监听
	 */
	private void checkboxItemActin() {
		for (int i = 0; i < items.length; i++)
			items[i].addItemListener(this);
	}

	/**
	 * list_btn触发事件
	 */
	private void btnListAction() {

		btnDownload.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 显示进度条
				downloadProgressDialog = new DownloadProgressDialog(ListDialog.this);
				downloadProgressDialog.setModal(true);
			}
		});
	}

	private void checkboxAllAction() {

		checkAllBox.addItemListener(this);
	}

	private void init() {
		//全选反选按钮
		checkAllBox = new JCheckBox(TextConstants.LISTDIALOG_FRAME_CHECKBOX_SELECTALL);
		//更改路径按钮
		btnChangeSavePath = new JButton(TextConstants.LISTDIALOG_FRAME_BUTTON_CHANGEDIR);
		//头部url信息

		titleLabel = new JLabel(TextConstants.LISTDIALOG_FRAME_LABEL_GET + ApplicationDatas.getPics().size() + TextConstants.LISTDIALOG_FRAME_LABEL_COUNT_1);

		titleLabel.setHorizontalAlignment(JLabel.CENTER);
		titleLabel.setPreferredSize(new Dimension(400, 30));
		selectedCountLabel = new JLabel(TextConstants.LISTDIALOG_FRAME_LABEL_CHOOSE + selectedCount + TextConstants.LISTDIALOG_FRAME_LABEL_COUNT_1);
		btnDownload = new JButton(TextConstants.LISTDIALOG_FRAME_BUTTON_DOWNLOAD_TEXT);
		btnDownload.setEnabled(false);
		//主面板
		contentPane = new JPanel();
		listPane = new JPanel();
		listPane.setLayout(new GridLayout(ApplicationDatas.getPics().size() + 1, 1));
		selectedCountLabel.setPreferredSize(new Dimension(150, 50));

		JScrollPane scrollPane = new JScrollPane(listPane, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		contentPane.add(titleLabel, BorderLayout.NORTH);
		items = new JCheckBox[ApplicationDatas.getPics().size()];
		Iterator<String> it = ApplicationDatas.getPics().iterator();
		for (int i = 0; it.hasNext(); i++) {
			JCheckBox checkBox = new JCheckBox(it.next());
			items[i] = checkBox;
			//checkBox.addItemListener(this);
			listPane.add(checkBox);
		}
		listPane.add(checkAllBox);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		bottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
		bottomPanel.add(selectedCountLabel);
		bottomPanel.add(btnDownload);
		contentPane.add(bottomPanel, BorderLayout.SOUTH);
		filePathLabel = new JLabel(ApplicationDatas.getSavePath());
		btnOpenLocalDir = new JButton(TextConstants.LISTDIALOG_FRAME_BUTTON_OPEN);
		bottomPanel.add(filePathLabel);
		bottomPanel.add(btnChangeSavePath);
		bottomPanel.add(btnOpenLocalDir);

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == checkAllBox) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				for (JCheckBox item : items) {
					item.setSelected(true);
				}
				checkAllBox.setText(TextConstants.LISTDIALOG_FRAME_CHECKBOX_DISSELECTALL);
			} else {
				for (JCheckBox item : items) {
					item.setSelected(false);
				}
				checkAllBox.setText(TextConstants.LISTDIALOG_FRAME_CHECKBOX_SELECTALL);
			}
		}
		//普通checkbox
		else {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				selectedCount += 1;
				ApplicationDatas.getSelectedPics().add(((JCheckBox) e.getSource()).getText());
				setBtnDownbloadAvialible(true);
			} else {
				selectedCount -= 1;
				ApplicationDatas.getSelectedPics().remove(((JCheckBox) e.getSource()).getText());
				setBtnDownbloadAvialible(false);
			}
		}
		selectedCountLabel.setText(TextConstants.LISTDIALOG_FRAME_LABEL_CHOOSE + selectedCount + TextConstants.LISTDIALOG_FRAME_LABEL_COUNT_1);
	}

	//开启与禁用下载按钮
	private void setBtnDownbloadAvialible(boolean isDisabled) {
		btnDownload.setEnabled(isDisabled);
	}
}
