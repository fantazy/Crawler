package com.fantazy.main;

import java.io.File;

/**
 * @Author Zhoufan [https://github.com/fantaga].
 * @Date 2016/12/30
 * @Description:
 */
public class SystemInfo {
	public static void main(String[] args) {
		System.out.println(System.getProperty("os.name") + " " + System.getProperty("os.version") + " " + System.getProperty("os.arch"));
		System.out.println(System.getProperty("user.home"));
		System.out.println(System.getProperty("line.separator"));
		System.out.print(File.separator);
	}
}
