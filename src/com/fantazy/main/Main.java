package com.fantazy.main;

import com.fantazy.model.ApplicationDatas;
import com.fantazy.task.CrawlerWorker;
import com.fantazy.util.SwingTools;
import com.fantazy.util.SystemTools;
import com.fantazy.util.TextConstants;
import com.fantazy.view.ListDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.logging.Logger;

/**
 * @author Fantazy :采用 GridBagLayout
 */
public class Main extends JFrame {

	private static Logger logger = Logger.getLogger(Main.class.getName());
	private static final int WIDTH = 640;
	private static final int HEIGHT = 360;
	public static JTextField input;
	private JButton ok;
	private JButton showlist;
	public static JTextArea display;

	private JPanel contentPane;
	static {
		logger.info(SystemTools.getOSName() + SystemTools.getOSVersion());
	}
	public Main() {
		super(TextConstants.MAIN_FRAME_TITLE);

		try {
			SwingTools.setSystemLookAndFeel();
		} catch (ClassNotFoundException e) {
			logger.warning(e.getMessage());
		} catch (InstantiationException e) {
			logger.warning(e.getMessage());
		} catch (IllegalAccessException e) {
			logger.warning(e.getMessage());
		} catch (UnsupportedLookAndFeelException e) {
			logger.warning(e.getMessage());
		}

		SwingTools.LocatedInCenter(this, WIDTH, HEIGHT);
		//初始化
		init();
		initData();
		//添加事件监听
		addActionListner();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	private void initData() {
		String savePath = SystemTools.getUserHome();
		ApplicationDatas.setSavePath(savePath);
	}

	/**
	 * 为组件添加事件监听
	 */
	private void addActionListner() {
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btn_ok_action(e);
			}
		});

		showlist.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btn_showlist_action();
			}

		});

		input.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				input_focus_lost(e);
			}

			@Override
			public void focusGained(FocusEvent e) {
				input_focus_gained(e);
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frame_close_action(e);
			}
		});
	}

	/**
	 * showlist按钮触发的事件函数
	 */
	private void btn_showlist_action() {
		if (ApplicationDatas.getPics() != null) {
			ListDialog listDialog = new ListDialog(this, true, TextConstants.LISTDIALOG_FRAME_TITLE);
		}

	}

	/**
	 * 初始化组件工作
	 */
	private void init() {
		//主面板
		contentPane = new JPanel();
		//输入框
		input = new JTextField(TextConstants.MAIN_FRAME_INPUT_LABLE_TEXT);
		//ok按钮
		ok = new JButton(TextConstants.MAIN_FRAME_BUTTON_GO_LABLE_TEXT);
		//输出
		display = new JTextArea();
		display.setEditable(false);
		JScrollPane scollPane = new JScrollPane(display);
		//选择列表
		showlist = new JButton(TextConstants.MAIN_FRAME_BUTTON_LIST_LABLE_TEXT);
		//工具面板
		JPanel toolPane = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 0));

		toolPane.add(showlist);

		GridBagLayout layout = new GridBagLayout();
		contentPane.setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		//输入框
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 0;
		gbc.insets = new Insets(10, 20, 0, 10);
		layout.setConstraints(input, gbc);
		contentPane.add(input);
		//GO按钮
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		gbc.weightx = 0;
		gbc.weighty = 0;
		layout.setConstraints(ok, gbc);
		contentPane.add(ok);

		//展示结果的TextArea
		gbc.insets = new Insets(10, 20, 0, 10);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		layout.setConstraints(scollPane, gbc);
		contentPane.add(scollPane);

		//toolPane
		gbc.insets = new Insets(10, 15, 10, 0);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;

		layout.setConstraints(toolPane, gbc);
		contentPane.add(toolPane);

		setContentPane(contentPane);
	}

	/**
	 * @param e 处理 ok 按钮事件函数 爬取 图像 文件 URL
	 */
	private void btn_ok_action(ActionEvent e) {

		logger.info(input.getText());
		CrawlerWorker cw = new CrawlerWorker();
		cw.execute();

	}

	private void input_focus_lost(FocusEvent e) {
		if (input.getText().trim().equals(TextConstants.MESSAGE_EMPTY) || input.getText() == null)
			input.setText(TextConstants.MAIN_FRAME_INPUT_LABLE_TEXT);
	}

	private void input_focus_gained(FocusEvent e) {
		if (input.getText().equals(TextConstants.MAIN_FRAME_INPUT_LABLE_TEXT))
			input.setText(TextConstants.MESSAGE_EMPTY);
	}

	private void frame_close_action(WindowEvent e) {
		int result = JOptionPane.showConfirmDialog(Main.this,
				TextConstants.MAIN_FRAME_SHUTDOWN_DIALOG_MESSAGE,
				TextConstants.MAIN_FRAME_SHUTDOWN_DIALOG_TITLE,
				JOptionPane.YES_NO_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE);
		System.out.println(result);
		if (result == JOptionPane.YES_OPTION) {
			System.exit(0);
		} else {
			setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Main frame = new Main();
				frame.setVisible(true);
			}
		});
	}
}
