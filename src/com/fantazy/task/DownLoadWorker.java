package com.fantazy.task;

import com.fantazy.model.ApplicationDatas;
import com.fantazy.model.DownLoad;
import com.fantazy.view.DownloadProgressDialog;

import javax.swing.*;
import java.util.List;
import java.util.logging.Logger;

public class DownLoadWorker extends SwingWorker<Integer, Integer> {

	private Logger logger = Logger.getLogger(DownLoadWorker.class.getName());
	private DownloadProgressDialog downloadProgressDialog;
	public  DownLoadWorker(DownloadProgressDialog dpd) {
		downloadProgressDialog = dpd;
	}
	
	@Override
	protected void process(List<Integer> chunks) {
		super.process(chunks);
		for(Integer i : chunks)
		{
			DownloadProgressDialog.progressBar.setValue((i * 100)/ ApplicationDatas.getSelectedPics().size());
		}
	}
	@Override
	protected Integer doInBackground() throws Exception {
		return doDownload();
	}
	@Override
	protected void done() {
		super.done();
		downloadProgressDialog.dispose();
		logger.info("done..");
	}

	private int doDownload(){
		int i = 0;
		DownLoad d = new DownLoad();
		for(String url : ApplicationDatas.getSelectedPics())
		{
			d.downLoad(url);
			i++;
			publish(i);
		}
		return i;
	}
}
